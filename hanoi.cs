using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hanoi
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduzca el numero de discos");
            int n = Convert.ToInt32(Console.ReadLine());
            hanoi(n, 1, 2, 3);
            Console.WriteLine("______ fin ______");
            Console.ReadKey();

        }
        public static void hanoi(int n,int origen, int auxiliar, int destino) {
            if (n == 1)
            {
                Console.WriteLine("mover disco de " + origen + " a " + destino);
                
            }
            else {
                hanoi(n-1,origen,destino,auxiliar);
                //mover todas las fichas menos la más grande (n) a la varilla auxiliar

                Console.WriteLine("mover disco de " + origen + " a " + destino);

                hanoi(n - 1, auxiliar, origen, destino);
                //mover todas las fichas restantes, 1...n–1, encima de la ficha grande (n)
            }
            Console.ReadKey();
        }
    }
}
